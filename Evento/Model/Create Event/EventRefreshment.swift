//
//  EventRefreshment.swift
//  Evento
//
//  Created by Faiq on 04/05/2021.
//

import Foundation

struct EventRefreshmentCategory {
    let name: String
    let imageName: String
}

struct EventRefreshmentItem {
    let id: String
    let name: String
    let category: String
    let media_uri: String
    var price: Int
}

struct RefreshmentCategoryCount {
    var starter: Int
    var mainCourse: Int
    var drinks: Int
    var dessert: Int
}
