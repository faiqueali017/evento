//
//  EventCategory.swift
//  Evento
//
//  Created by Faiq on 04/05/2021.
//

import Foundation

struct EventCategory {
    let name: String
    let media_uri : String
    let description: String
}
