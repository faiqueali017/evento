//
//  NewEvent.swift
//  Evento
//
//  Created by Faiq on 06/05/2021.
//

import Foundation

struct NewEvent {
    var user_id: String
    var event_id: String
    var event_category: String
    var event_venue_id: String
    var event_timestamp: Int
    var event_venue_cost: Int
    var event_persons: Int
    var event_refreshments_id: [String]
    var event_refreshments_cost: Double
    var event_refreshments_items_count: Int
    var event_total_cost: Double
    var event_advance_percent: Int
    var event_advance_paid: Double
    var event_name: String
    var event_description: String
}
