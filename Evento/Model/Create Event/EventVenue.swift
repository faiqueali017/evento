//
//  EventVenue.swift
//  Evento
//
//  Created by Faiq on 04/05/2021.
//

import Foundation
import Firebase

struct EventVenue {
    let id: String
    let name: String
    let address: String
    let contact_no: String
    let description: String
    let media_uri: String
    let price: Int
    let timings: String
    let coordinates: GeoPoint
}

