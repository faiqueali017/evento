//
//  UIImageView+.swift
//  Evento
//
//  Created by Faiq on 04/05/2021.
//

import Foundation
import UIKit
import Firebase

extension UIImageView {
    
    func loadImage(url: URL) {
        Auth.auth().signInAnonymously() { (authResult, error) in
            DispatchQueue.global().async {
                [weak self] in
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self?.image = image
                        }
                    }
                }
            }
        }
    }
    
    func loadImageFromUrlString(urlString: String) {
        image = nil
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
    
}
