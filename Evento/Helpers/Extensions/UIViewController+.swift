//
//  UIViewController+.swift
//  Evento
//
//  Created by Faiq on 25/04/2021.
//

import Foundation
import UIKit

extension UIViewController {
    
    //To Hide keyboard when tapped around
    func hideKeyboardWhenTappedAround(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
