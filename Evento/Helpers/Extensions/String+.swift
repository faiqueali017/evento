//
//  String+.swift
//  Evento
//
//  Created by Faiq on 04/05/2021.
//

import Foundation

extension String {
    var firstCapitalized: String {
        return prefix(1).capitalized + dropFirst()
    }
}
