//
//  Constants.swift
//  Evento
//
//  Created by Faiq on 02/04/2021.
//

import Foundation

struct Constants {
    
    struct STORYBOARD {
        static let HOME_VIEW_CONTROLLER_ID = "HomeVC"
        static let TAB_BAR_CONTROLLER_ID = "MainTabBarController"
        static let ADMIN_TAB_CONTROLLER_ID = "AdminTabBarController"
        static let PICK_EVENT_VENUE_CONTROLLER_ID = "PickEventVenueViewController"
        static let PICK_VENUE_DETAIL_CONTROLLER_ID = "EventVenueDetailViewController"
        static let CHOOSE_NO_OF_PERSONS_CONTROLLER_ID = "ChooseNoOfPersonsViewController"
        static let REFRESHMENTS_CONTROLLER_ID = "RefreshmentsViewController"
        static let REFRESHMENTS_CHECKOUT_CONTROLLER_ID = "RefreshmentsCheckoutViewController"
        static let PROFILE_EDIT_CONTROLLER_ID = "ProfileEditViewController"
        static let EDIT_NAME_CONTROLLER_ID = "EditNameViewController"
        static let BILL_CHECKOUT_CONTROLLER_ID = "BillCheckoutTableViewController"
        static let EVENT_CONFIRM_CONTROLLER_ID = "EventConfirmViewController"
        static let LOGIN_NAVIGATION_CONTROLLER = "LoginNavigationController"
    }
    
    struct USER_DEFAULTS {
        struct KEYS {
            static let USER_SIGNED_IN = "USER_SIGNED_IN"
            static let ADMIN_SIGNED_IN = "ADMIN_SIGNED_IN"
        }
    }
    
    struct APP_STATE_PROPERTIES {
        static let USER_SIGNED_IN = "SUCCESSFULL"
        static let ADMIN_SIGNED_IN = "SUCCESSFULL"
    }
    
    struct FIRESTORE_REF {
        static let EVENT_CATEGORY = "event_category"
        static let EVENT_REFRESHMENTS = "refreshments"
        static let NEW_EVENT = "new_events"
        static let PICNIC = "picnic"
        static let REFRESHMENTS = "refreshments"
        static let CONFERENCE = "conference"
        static let MEETING = "meeting"
    }
    
    struct ADMIN_CREDENTIALS {
        static let EMAIL = "admin@evento.com"
        static let PASSWORD = "admin@123"
    }
    
}
