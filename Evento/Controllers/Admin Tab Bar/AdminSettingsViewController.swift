//
//  AdminSettingsViewController.swift
//  Evento
//
//  Created by Faiq on 07/05/2021.
//

import UIKit

class AdminSettingsViewController: UIViewController {

    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logoutButton.layer.cornerRadius = 25
        logoutButton.dropShadow(shadowOpacity: 0.15, shadowRadius: 9, shadowColor: UIColor.black.cgColor)
    }
    

    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        //after successfull logout
        UserDefaults.standard.removeObject(forKey: Constants.USER_DEFAULTS.KEYS.ADMIN_SIGNED_IN)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.LOGIN_NAVIGATION_CONTROLLER)
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    
}
