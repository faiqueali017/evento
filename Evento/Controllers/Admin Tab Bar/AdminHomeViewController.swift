//
//  AdminHomeViewController.swift
//  Evento
//
//  Created by Faiq on 07/05/2021.
//

import UIKit
import Firebase

class AdminHomeViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var allEventsTableView: UITableView!
    
    //MARK:- Variables
    private var allEvents = [NewEvent]()
    
    //MARK:- Constant
    private let db = Firestore.firestore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewXibs()
        fetchAllEvents()
    }
    
    //MARK:- Helper Methods
    private func registerTableViewXibs(){
        allEventsTableView.register(UINib(nibName: "NoEventsTableViewCell", bundle: nil), forCellReuseIdentifier: NoEventsTableViewCell.reuseIdentifier)
        
        allEventsTableView.register(UINib(nibName: "MyEventsTableViewCell", bundle: nil), forCellReuseIdentifier: MyEventsTableViewCell.reuseIdentifier)
    }
    
    private func fetchAllEvents(){
        let ref = db.collection(Constants.FIRESTORE_REF.NEW_EVENT)
        
        ref.getDocuments { (snapshot, error) in
            if let err = error {
                debugPrint("Error fetching docs: \(err)")
            } else {
                guard let snap = snapshot else {return}
                for document in snap.documents {
                    let data = document.data()
                    
                    let user_id = data["user_id"] as? String ?? ""
                    let event_id = data["event_id"] as? String ?? ""
                    let event_category = data["event_category"] as? String ?? ""
                    let event_venue_id = data["event_venue_id"] as? String ?? ""
                    let event_timestamp = data["event_timestamp"] as? Int ?? 0
                    let event_venue_cost = data["event_venue_cost"] as? Int ?? 0
                    let event_persons = data["event_persons"] as? Int ?? 0
                    let event_refreshments_id = data["event_refreshments_id"] as? [String] ?? [String]()
                    let event_refreshments_cost = data["event_refreshments_cost"] as? Double ?? 0.0
                    let event_refreshments_items_count = data["event_refreshments_items_count"] as? Int ?? 0
                    let event_total_cost = data["event_total_cost"] as? Double ?? 0.0
                    let event_advance_percent = data["event_advance_percent"] as? Int ?? 0
                    let event_advance_paid = data["event_advance_paid"] as? Double ?? 0.0
                    let event_name = data["event_name"] as? String ?? ""
                    let event_description = data["event_description"] as? String ?? ""
                    
                    
                    let event = NewEvent(user_id: user_id,
                                         event_id: event_id,
                                         event_category: event_category,
                                         event_venue_id: event_venue_id ,
                                         event_timestamp: event_timestamp,
                                         event_venue_cost: event_venue_cost,
                                         event_persons: event_persons,
                                         event_refreshments_id: event_refreshments_id,
                                         event_refreshments_cost: event_refreshments_cost,
                                         event_refreshments_items_count: event_refreshments_items_count,
                                         event_total_cost: event_total_cost,
                                         event_advance_percent: event_advance_percent,
                                         event_advance_paid: event_advance_paid,
                                         event_name: event_name,
                                         event_description: event_description)
                    
                    self.allEvents.append(event)
                }
                self.allEventsTableView.reloadData()
            }
        }
    }
}

//MARK:- TableView Extensions
extension AdminHomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allEvents.count == 0 {
            return 1
        } else {
            return allEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if allEvents.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: NoEventsTableViewCell.reuseIdentifier, for: indexPath) as! NoEventsTableViewCell
            cell.configureEventDescLabelTextForAdmin()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MyEventsTableViewCell.reuseIdentifier, for: indexPath) as! MyEventsTableViewCell
            cell.configureCell(event: allEvents[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if allEvents.count > 0 {
            tableView.deselectRow(at: indexPath, animated: false)
            print(indexPath.row)
            //Present my event detail Screen
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if allEvents.count == 0 {
            return 300
        } else {
            return 145
        }
    }
 
}

