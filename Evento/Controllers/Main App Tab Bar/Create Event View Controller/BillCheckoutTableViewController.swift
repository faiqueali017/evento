//
//  BillCheckoutTableViewController.swift
//  Evento
//
//  Created by Faiq on 05/05/2021.
//

import UIKit

class BillCheckoutTableViewController: UITableViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var totalVenueCostLabel: UILabel!
    @IBOutlet weak var totalRefreshmentCostLabel: UILabel!
    @IBOutlet weak var totalEventCostLabel: UILabel!
    
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var percentStepper: UIStepper!
    
    @IBOutlet weak var totalEventCostLabel1: UILabel!
    @IBOutlet weak var totalEventPercentLabel: UILabel!
    @IBOutlet weak var totalEventRemainingBillLabel: UILabel!
    @IBOutlet weak var minusPercentLabel: UILabel!
    
    //MARK:- Variables
    var eventTotalCost = Double()
    private var event = EventConfirmViewController.newEvent
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalVenueCostLabel.text = String(event.event_venue_cost)
        totalRefreshmentCostLabel.text = String(event.event_refreshments_cost)
        totalEventCostLabel.text = String(Utilities.getTotalEventCost(totalVenueCostLabel, refreshmentCostLabel: totalRefreshmentCostLabel))
            
        totalEventCostLabel1.text = String(Utilities.getTotalEventCost(totalVenueCostLabel, refreshmentCostLabel: totalRefreshmentCostLabel))
        totalEventPercentLabel.text = "0"
        totalEventRemainingBillLabel.text = String(Utilities.getTotalEventCost(totalVenueCostLabel, refreshmentCostLabel: totalRefreshmentCostLabel))
        
        eventTotalCost = Utilities.getTotalEventCost(totalVenueCostLabel, refreshmentCostLabel: totalRefreshmentCostLabel)
        
    }
    
    @IBAction func didTapStepper(_ sender: UIStepper) {
        percentLabel.text = String(Int(sender.value))
        minusPercentLabel.text = "- " + String(Int(sender.value)) + " %"
        
        let reducePrice = Double(eventTotalCost) * sender.value / 100
        totalEventPercentLabel.text = String(reducePrice)
        
        totalEventRemainingBillLabel.text = String(Int(eventTotalCost - Double(Int(reducePrice))))
    }
    
    @IBAction func didTapFinishButton(_ sender: Any) {
        //Setting event refreshments details into struct
        EventConfirmViewController.newEvent.event_total_cost = Utilities.double(totalEventCostLabel)
        EventConfirmViewController.newEvent.event_advance_percent = Utilities.integer(percentLabel)
        EventConfirmViewController.newEvent.event_advance_paid = Utilities.double(totalEventPercentLabel)
        
        //presenting to new screen
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.EVENT_CONFIRM_CONTROLLER_ID)
        //self.present(searchSymptomsViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1{
            return 1
        } else if section == 2 {
            return 3
        } else if section == 3 {
            return 1
        } else {
            return 0
        }
    }

    

}
