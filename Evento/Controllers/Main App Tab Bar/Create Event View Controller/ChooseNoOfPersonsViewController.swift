//
//  ChooseNoOfPersonsViewController.swift
//  Evento
//
//  Created by Faiq on 24/04/2021.
//

import UIKit

class ChooseNoOfPersonsViewController: UIViewController {
    
    private let personsTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "For how many persons would you like to host an event?"
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .label
        //label.backgroundColor = .red
        return label
    }()
    
    private let personsDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 17)
        label.text = "The Venue which you have selected has a capacity to host a Minimum of 25 to a Maximum of 200 persons."
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .label
        //label.backgroundColor = .red
        return label
    }()
    
    private let personsTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Enter no. of Persons"
        tf.keyboardType = .asciiCapableNumberPad
        tf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return tf
    }()
    
    private let personsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 22)
        label.text = "Persons:"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 17)
        label.text = "Your entered no. of persons exceeding capacity"
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .red
        label.isHidden = true
        return label
    }()
    
    private let nextButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Next", for: .normal)
        button.backgroundColor = .systemBlue
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 22)
        button.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        self.hideKeyboardWhenTappedAround()
        
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        //Add title label
        view.addSubview(personsTitleLabel)
        personsTitleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        personsTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        personsTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        //Add desc label
        view.addSubview(personsDescLabel)
        personsDescLabel.topAnchor.constraint(equalTo: personsTitleLabel.bottomAnchor, constant: 5).isActive = true
        personsDescLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        personsDescLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        //Add text field
        view.addSubview(personsTextField)
        personsTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        personsTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        personsTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        //Add persons label
        view.addSubview(personsLabel)
        personsLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        personsLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        personsLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -30).isActive = true
        
        //Add error label
        view.addSubview(errorLabel)
        errorLabel.topAnchor.constraint(equalTo: personsTextField.bottomAnchor, constant: 10).isActive = true
        errorLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        errorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        //Adding Next Btn
        view.addSubview(nextButton)
        nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        nextButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nextButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        
        
    }
    
    //MARK:- objc Methods
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let input = textField.text {
            let i = Int(input)
            if let count = i {
                if count < 20 || count > 100{
                    errorLabel.isHidden = false
                    nextButton.isHidden = true
                } else {
                    errorLabel.isHidden = true
                    nextButton.isHidden = false
                }
            }
        }
    }
    
    @objc func didTapNextButton() {
        //Setting event persons into struct
        EventConfirmViewController.newEvent.event_persons = Utilities.integer(personsTextField)
        
        //Presenting new screen
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let refreshmentsViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.REFRESHMENTS_CONTROLLER_ID)
        //self.present(searchSymptomsViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(refreshmentsViewController, animated: true)
        
    }

}
