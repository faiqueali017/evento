//
//  EventVenueDetailViewController.swift
//  Evento
//
//  Created by Faiq on 23/04/2021.
//

import UIKit
import MapKit

class EventVenueDetailViewController: UIViewController {
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.showsVerticalScrollIndicator = false
        //view.backgroundColor = .systemBackground
        return view
    }()
    
    /// VENUE HEADER VIEW
    private let venueHeaderView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let venueMap: MKMapView = {
        let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
    }()
    
    private let venueImageBackgroundView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let venueImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.masksToBounds = false
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    /// VENUE MID VIEW
    private let venueMidView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    let venueNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "Turtle Park"
        label.numberOfLines = 0
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    let venueAddressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 21)
        label.text = "Joshua Tree National park, California"
        label.numberOfLines = 0
        if #available(iOS 13.0, *) {
            label.textColor = .darkGray
        } else {
            label.textColor = .black
        }
        //label.backgroundColor = .red
        return label
    }()
    
    let venueMidViewSeparatorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .lightGray
        return label
    }()
    
    /// VENUE FOOTEER VIEW
    private let venueFooterView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let timingsIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .yellow
        return image
    }()
    
    private let timingsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "10:00 AM - 10:00PM"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let chargesIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .yellow
        return image
    }()
    
    private let chargesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "200,000"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let facilitiesIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .yellow
        return image
    }()
    
    private let facilitiesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let contactIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .yellow
        return image
    }()

    private let contactLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "+92-3333333333"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    ///BOOK YOUR SLOTS VIEW
    private let venueBookingSlotView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let bookYourSlotsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 22)
        label.text = "Book your slots"
        label.numberOfLines = 0
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let bookYourSlotsDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "Check availability of venue on your desired date and time."
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let datePicker: UIDatePicker = UIDatePicker()
    
    private let checkAvailabilityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "Sorry, the venue is not available on your selected date and time."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .systemRed
        label.isHidden = true
        return label
    }()
    
    private let checkAvailabilityButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Check Availability", for: .normal)
        button.backgroundColor = .systemBlue
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 22)
        button.addTarget(self, action: #selector(didTapCheckAvailabilityButton), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    //MARK:- Variables
    static var selectedEventVenue: EventVenue?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        fillDataInScreen()
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        configureVenueHeaderViewUI()
        
        configureVenueMidViewUI()
        
        configureVenueFooterViewUI()
        
        configureVenueBookingSlotsViewUI()
    }
    
    private func configureVenueHeaderViewUI(){
        scrollView.addSubview(venueHeaderView)
        venueHeaderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        venueHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        venueHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        venueHeaderView.heightAnchor.constraint(equalToConstant: 420 ).isActive = true
        
        ///ADDING MAP
        venueHeaderView.addSubview(venueMap)
        venueMap.topAnchor.constraint(equalTo: venueHeaderView.topAnchor).isActive = true
        venueMap.leadingAnchor.constraint(equalTo: venueHeaderView.leadingAnchor).isActive = true
        venueMap.trailingAnchor.constraint(equalTo: venueHeaderView.trailingAnchor).isActive = true
        venueMap.heightAnchor.constraint(equalToConstant: 270).isActive = true
        
        configureVenueMap()
        
        ///ADDING VENUE IMAGE-BACKGROUND
        venueHeaderView.addSubview(venueImageBackgroundView)
        venueImageBackgroundView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        venueImageBackgroundView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        venueImageBackgroundView.centerXAnchor.constraint(equalTo: venueHeaderView.centerXAnchor).isActive = true
        venueImageBackgroundView.centerYAnchor.constraint(equalTo: venueMap.bottomAnchor).isActive = true
        venueImageBackgroundView.layer.cornerRadius = 250/2
        
        //Add background view shadow
        venueImageBackgroundView.dropShadow(shadowOpacity: 0.3, shadowRadius: 12, shadowColor: UIColor.black.cgColor)
        
        
        ///ADDING VENUE IMAGE
        venueImageBackgroundView.addSubview(venueImageView)
        venueImageView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        venueImageView.widthAnchor.constraint(equalToConstant: 240).isActive = true
        venueImageView.centerXAnchor.constraint(equalTo: venueImageBackgroundView.centerXAnchor).isActive = true
        venueImageView.centerYAnchor.constraint(equalTo: venueImageBackgroundView.centerYAnchor).isActive = true
        venueImageView.layer.cornerRadius = 240/2
        
    }
    
    private func configureVenueMap(){
//        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
//        venueMap.centerToLocation(initialLocation)
        
        let latitude = EventVenueDetailViewController.selectedEventVenue?.coordinates.latitude ?? 0.000000
        let longitude = EventVenueDetailViewController.selectedEventVenue?.coordinates.longitude ?? 0.000000
        
        let initialLocation = CLLocation(latitude: latitude, longitude: longitude)
        venueMap.centerToLocation(initialLocation)
    }
    
    private func configureVenueMidViewUI(){
        
        scrollView.addSubview(venueMidView)
        venueMidView.topAnchor.constraint(equalTo: venueHeaderView.bottomAnchor).isActive = true
        venueMidView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        venueMidView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        venueMidView.heightAnchor.constraint(equalToConstant: 128).isActive = true
        
        ///ADDING VENUE NAME LABEL
        venueMidView.addSubview(venueNameLabel)
        venueNameLabel.topAnchor.constraint(equalTo: venueMidView.topAnchor, constant: 5).isActive = true
        venueNameLabel.leadingAnchor.constraint(equalTo: venueMidView.leadingAnchor, constant: 15).isActive = true
        venueNameLabel.trailingAnchor.constraint(equalTo: venueMidView.trailingAnchor, constant: -15).isActive = true
        
        ///ADDING VENUE DESC LABEL
        venueMidView.addSubview(venueAddressLabel)
        venueAddressLabel.topAnchor.constraint(equalTo: venueNameLabel.bottomAnchor).isActive = true
        venueAddressLabel.leadingAnchor.constraint(equalTo: venueMidView.leadingAnchor, constant: 15).isActive = true
        venueAddressLabel.trailingAnchor.constraint(equalTo: venueMidView.trailingAnchor, constant: -15).isActive = true
        
        ///ADDING SEPARATOR LINE
        venueMidView.addSubview(venueMidViewSeparatorLabel)
        venueMidViewSeparatorLabel.topAnchor.constraint(equalTo: venueAddressLabel.bottomAnchor, constant: 5).isActive = true
        venueMidViewSeparatorLabel.leadingAnchor.constraint(equalTo: venueMidView.leadingAnchor, constant: 15).isActive = true
        venueMidViewSeparatorLabel.trailingAnchor.constraint(equalTo: venueMidView.trailingAnchor, constant: -15).isActive = true
        venueMidViewSeparatorLabel.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    private func configureVenueFooterViewUI(){
        scrollView.addSubview(venueFooterView)
        venueFooterView.topAnchor.constraint(equalTo: venueMidView.bottomAnchor).isActive = true
        venueFooterView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        venueFooterView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        venueFooterView.heightAnchor.constraint(equalToConstant: 370).isActive = true
        //venueFooterView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        //ADDING FACILITIES ICON
        venueFooterView.addSubview(facilitiesIcon)
        facilitiesIcon.topAnchor.constraint(equalTo: venueFooterView.topAnchor, constant: 10).isActive = true
        facilitiesIcon.leadingAnchor.constraint(equalTo: venueFooterView.leadingAnchor, constant: 15).isActive = true
        facilitiesIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        facilitiesIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        venueFooterView.addSubview(facilitiesLabel)
        facilitiesLabel.topAnchor.constraint(equalTo: venueFooterView.topAnchor, constant: 10).isActive = true
        facilitiesLabel.leadingAnchor.constraint(equalTo: facilitiesIcon.trailingAnchor, constant: 5).isActive = true
        facilitiesLabel.trailingAnchor.constraint(equalTo: venueFooterView.trailingAnchor, constant: -15).isActive = true
        
        
        //ADDING TIMINGS ICON
        venueFooterView.addSubview(timingsIcon)
        timingsIcon.topAnchor.constraint(equalTo: facilitiesLabel.bottomAnchor, constant: 15).isActive = true
        timingsIcon.leadingAnchor.constraint(equalTo: venueFooterView.leadingAnchor, constant: 15).isActive = true
        timingsIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        timingsIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        venueFooterView.addSubview(timingsLabel)
        timingsLabel.topAnchor.constraint(equalTo: facilitiesLabel.bottomAnchor, constant: 15).isActive = true
        timingsLabel.leadingAnchor.constraint(equalTo: timingsIcon.trailingAnchor, constant: 5).isActive = true
        timingsLabel.trailingAnchor.constraint(equalTo: venueFooterView.trailingAnchor, constant: -15).isActive = true
        
        
        //ADDING CHARGES ICON
        venueFooterView.addSubview(chargesIcon)
        chargesIcon.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 15).isActive = true
        chargesIcon.leadingAnchor.constraint(equalTo: venueFooterView.leadingAnchor, constant: 15).isActive = true
        chargesIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        chargesIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        venueFooterView.addSubview(chargesLabel)
        chargesLabel.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 15).isActive = true
        chargesLabel.leadingAnchor.constraint(equalTo: chargesIcon.trailingAnchor, constant: 5).isActive = true
        chargesLabel.trailingAnchor.constraint(equalTo: venueFooterView.trailingAnchor, constant: -15).isActive = true
        
        //ADDING CHARGES ICON
        venueFooterView.addSubview(chargesIcon)
        chargesIcon.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 15).isActive = true
        chargesIcon.leadingAnchor.constraint(equalTo: venueFooterView.leadingAnchor, constant: 15).isActive = true
        chargesIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        chargesIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        venueFooterView.addSubview(chargesLabel)
        chargesLabel.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 15).isActive = true
        chargesLabel.leadingAnchor.constraint(equalTo: chargesIcon.trailingAnchor, constant: 5).isActive = true
        chargesLabel.trailingAnchor.constraint(equalTo: venueFooterView.trailingAnchor, constant: -15).isActive = true
        
        //ADDING CONTACT ICON
        venueFooterView.addSubview(contactIcon)
        contactIcon.topAnchor.constraint(equalTo: chargesIcon.bottomAnchor, constant: 15).isActive = true
        contactIcon.leadingAnchor.constraint(equalTo: venueFooterView.leadingAnchor, constant: 15).isActive = true
        contactIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        contactIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        venueFooterView.addSubview(contactLabel)
        contactLabel.topAnchor.constraint(equalTo: chargesIcon.bottomAnchor, constant: 15).isActive = true
        contactLabel.leadingAnchor.constraint(equalTo: contactIcon.trailingAnchor, constant: 5).isActive = true
        contactLabel.trailingAnchor.constraint(equalTo: venueFooterView.trailingAnchor, constant: -15).isActive = true
    }
    
    private func configureVenueBookingSlotsViewUI(){
        scrollView.addSubview(venueBookingSlotView)
        venueBookingSlotView.topAnchor.constraint(equalTo: venueFooterView.bottomAnchor).isActive = true
        venueBookingSlotView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        venueBookingSlotView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        venueBookingSlotView.heightAnchor.constraint(equalToConstant: 260).isActive = true
        venueBookingSlotView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        //Add book slots label
        venueBookingSlotView.addSubview(bookYourSlotsLabel)
        bookYourSlotsLabel.topAnchor.constraint(equalTo: venueBookingSlotView.topAnchor, constant: 10).isActive = true
        bookYourSlotsLabel.leadingAnchor.constraint(equalTo: venueBookingSlotView.leadingAnchor, constant: 15).isActive = true
        bookYourSlotsLabel.trailingAnchor.constraint(equalTo: venueBookingSlotView.trailingAnchor, constant: -15).isActive = true
        bookYourSlotsLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        //Add availability label
        venueBookingSlotView.addSubview(bookYourSlotsDescLabel)
        bookYourSlotsDescLabel.topAnchor.constraint(equalTo: bookYourSlotsLabel.bottomAnchor, constant: 1).isActive = true
        bookYourSlotsDescLabel.leadingAnchor.constraint(equalTo: venueBookingSlotView.leadingAnchor, constant: 15).isActive = true
        bookYourSlotsDescLabel.trailingAnchor.constraint(equalTo: venueBookingSlotView.trailingAnchor, constant: -15).isActive = true
        bookYourSlotsDescLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Add date picker
        venueBookingSlotView.addSubview(datePicker)
        
        datePicker.frame = CGRect(x: 15, y: 95, width: 200, height: 40)
        
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = .systemBackground
        
        datePicker.addTarget(self, action: #selector(EventVenueDetailViewController.datePickerValueChanged(_:)), for: .valueChanged)
        
        //Add availability desc label
        venueBookingSlotView.addSubview(checkAvailabilityLabel)
        checkAvailabilityLabel.topAnchor.constraint(equalTo: datePicker.bottomAnchor, constant: 1).isActive = true
        checkAvailabilityLabel.leadingAnchor.constraint(equalTo: venueBookingSlotView.leadingAnchor, constant: 15).isActive = true
        checkAvailabilityLabel.trailingAnchor.constraint(equalTo: venueBookingSlotView.trailingAnchor, constant: -15).isActive = true
        checkAvailabilityLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Add availability button
        venueBookingSlotView.addSubview(checkAvailabilityButton)
        checkAvailabilityButton.topAnchor.constraint(equalTo: checkAvailabilityLabel.bottomAnchor, constant: 5).isActive = true
        checkAvailabilityButton.leadingAnchor.constraint(equalTo: venueBookingSlotView.leadingAnchor, constant: 35).isActive = true
        checkAvailabilityButton.trailingAnchor.constraint(equalTo: venueBookingSlotView.trailingAnchor, constant: -35).isActive = true
        checkAvailabilityButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        checkAvailabilityButton.layer.cornerRadius = 10
    }
    
    private func fillDataInScreen(){
        if let venue = EventVenueDetailViewController.selectedEventVenue {
            venueNameLabel.text = venue.name
            venueAddressLabel.text = venue.address
            timingsLabel.text = venue.timings
            facilitiesLabel.text = venue.description
            chargesLabel.text = String(venue.price)
            contactLabel.text = venue.contact_no
            
            if let imageURL = URL(string: venue.media_uri) {
                venueImageView.loadImage(url: imageURL)
            }
            
            //venueMap
        }
    }
    
    //MARK:- objc Methods
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
//
//        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        //print("Selected Value \(Int(datePicker.date.timeIntervalSince1970))")
        EventConfirmViewController.newEvent.event_timestamp = Int(datePicker.date.timeIntervalSince1970)
    }
    
    @objc func didTapCheckAvailabilityButton() {
        checkAvailabilityLabel.isHidden = false
    }
    
}

