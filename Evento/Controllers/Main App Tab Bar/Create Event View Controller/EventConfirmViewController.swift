//
//  EventConfirmViewController.swift
//  Evento
//
//  Created by Faiq on 01/05/2021.
//

import UIKit
import Firebase

class EventConfirmViewController: UIViewController {

    @IBOutlet weak var eventNameTextField: UITextField!
    @IBOutlet weak var eventDescTextView: UITextView!
    
    static var newEvent = NewEvent(user_id: Auth.auth().currentUser?.uid ?? "",
                                         event_id: String(),
                                         event_category: String(),
                                         event_venue_id: String(), event_timestamp: Int(),
                                         event_venue_cost: Int(),
                                         event_persons: Int(),
                                         event_refreshments_id: [String()],
                                         event_refreshments_cost: Double(),
                                         event_refreshments_items_count: Int(),
                                         event_total_cost: Double(),
                                         event_advance_percent: Int(),
                                         event_advance_paid: Double(),
                                         event_name: String(),
                                         event_description: String())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    @IBAction func didTapDoneButton(_ sender: Any) {
        //Initialize a Hud view obj
        let hudView = HudView.hud(inView: self.view, animated: true)
        
        //Set hud view text
        hudView.text = NSLocalizedString("Created", comment: "Created")
        
        savingNewEventDetailsToFirestore()
        
        //Dismiss hud view timings
        hudView.afterDelay(0.8) {
            hudView.hide()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func savingNewEventDetailsToFirestore(){
        
        //Creating roots doc ref
        let newEvent = Firestore.firestore().collection(Constants.FIRESTORE_REF.NEW_EVENT)
        
        EventConfirmViewController.newEvent.event_name = eventNameTextField.text ?? ""
        EventConfirmViewController.newEvent.event_description = eventDescTextView.text ?? ""
        EventConfirmViewController.newEvent.event_id = newEvent.document().documentID
        
        //Saving event on firebase
        let event = EventConfirmViewController.newEvent
        newEvent.document(event.event_id).setData(
            ["user_id": event.user_id,
             "event_id": event.event_id,
             "event_category": event.event_category,
             "event_venue_id": event.event_venue_id,
             "event_timestamp": event.event_timestamp,
             "event_venue_cost": event.event_venue_cost,
             "event_persons": event.event_persons,
             "event_refreshments_id": event.event_refreshments_id,
             "event_refreshments_cost": event.event_refreshments_cost,
             "event_refreshments_items_count": event.event_refreshments_items_count,
             "event_total_cost": event.event_total_cost,
             "event_advance_percent": event.event_advance_percent,
             "event_advance_paid": event.event_advance_paid,
             "event_name": event.event_name,
             "event_description": event.event_description
            ])
    }
    
    
}
