//
//  RefreshmentsViewController.swift
//  Evento
//
//  Created by Faiq on 25/04/2021.
//

import UIKit
import Firebase

class RefreshmentsViewController: UIViewController {
    
    //MARK:- UIViews
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.showsVerticalScrollIndicator = false
        //view.backgroundColor = .systemBackground
        return view
    }()
    
    /// REFRESHMENTS HEADER VIEW
    private let refreshmentsHeaderView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let refreshmentsTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "Select refreshments / food for you attendies"
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .label
        //label.backgroundColor = .red
        return label
    }()
    
    private let refreshmentsDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 17)
        label.text = "We offer a huge variety of food range. You can go with whatever you like."
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .label
        //label.backgroundColor = .red
        return label
    }()
    
    /// REFRESHMENTS MID VIEW
    private let refreshmentsCategoryView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private var refreshmentsCategoryCollectionView: UICollectionView?
    
    /// REFRESHMENTS FOOTEER VIEW
    private let refreshmentsFooterView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let categoryNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 25)
        label.text = "All"
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private var refreshmentsItemsCollectionView: UICollectionView?
    
    private let myMenuButton: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = .systemOrange
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private let myMenuIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = #colorLiteral(red: 1, green: 0.9327824712, blue: 0.7743675113, alpha: 1)
        return image
    }()
    
    private let myMenuCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 17)
        label.text = "0 items"
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .white
        return label
    }()
    
    //MARK:- Variables
    private var selectedRefreshmentItemsCount = Int()
    private var refreshmentsCollectionRef: CollectionReference!
    private var refreshmentAllItems = [EventRefreshmentItem]()
    private var refreshmentSelectedCategoryItems = [EventRefreshmentItem]()
    private var refreshmentCategories = [EventRefreshmentCategory(name: "All", imageName: "All"),
                                         EventRefreshmentCategory(name: "Starter", imageName: "All"),
                                         EventRefreshmentCategory(name: "Burgers", imageName: "All"),
                                         EventRefreshmentCategory(name: "Noodles", imageName: "All"),
                                         EventRefreshmentCategory(name: "Dessert", imageName: "All"),
                                         EventRefreshmentCategory(name: "Drinks", imageName: "All"),
                                         EventRefreshmentCategory(name: "Hot Drinks", imageName: "All")]

    override func viewDidLoad() {
        super.viewDidLoad()
        //Setting up UI
        setupUI()
        
        //Setting firebase root ref
        refreshmentsCollectionRef = Firestore.firestore().collection(Constants.FIRESTORE_REF.EVENT_REFRESHMENTS)
        
        //Fetching refreshment Items from firebase
        fetchAllRefreshments()
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        configureRefreshmentsHeaderViewUI()
        
        configureRefreshmentsMidViewUI()
        
        configureRefreshmentsFooterViewUI()
        
        configureMyMenuButtonUI()
    }
    
    private func configureRefreshmentsHeaderViewUI(){
        scrollView.addSubview(refreshmentsHeaderView)
        refreshmentsHeaderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        refreshmentsHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        refreshmentsHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        refreshmentsHeaderView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        //Add title label
        refreshmentsHeaderView.addSubview(refreshmentsTitleLabel)
        refreshmentsTitleLabel.topAnchor.constraint(equalTo: refreshmentsHeaderView.topAnchor, constant: 20).isActive = true
        refreshmentsTitleLabel.leadingAnchor.constraint(equalTo: refreshmentsHeaderView.leadingAnchor, constant: 20).isActive = true
        refreshmentsTitleLabel.trailingAnchor.constraint(equalTo: refreshmentsHeaderView.trailingAnchor, constant: -20).isActive = true
        
        //Add desc label
        refreshmentsHeaderView.addSubview(refreshmentsDescLabel)
        refreshmentsDescLabel.topAnchor.constraint(equalTo: refreshmentsTitleLabel.bottomAnchor, constant: 5).isActive = true
        refreshmentsDescLabel.leadingAnchor.constraint(equalTo: refreshmentsHeaderView.leadingAnchor, constant: 20).isActive = true
        refreshmentsDescLabel.trailingAnchor.constraint(equalTo: refreshmentsHeaderView.trailingAnchor, constant: -20).isActive = true
        
    }
    
    private func configureRefreshmentsMidViewUI(){
        
        scrollView.addSubview(refreshmentsCategoryView)
        refreshmentsCategoryView.topAnchor.constraint(equalTo: refreshmentsHeaderView.bottomAnchor).isActive = true
        refreshmentsCategoryView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        refreshmentsCategoryView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        refreshmentsCategoryView.heightAnchor.constraint(equalToConstant: 135).isActive = true
        
        setupRefreshmentCategoryCollectionView(refreshmentsCategoryView)
    }
    
    private func setupRefreshmentCategoryCollectionView(_ view: UIView) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 5)
        refreshmentsCategoryCollectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        refreshmentsCategoryCollectionView?.showsHorizontalScrollIndicator = false
        
        //register Xib
        let nib = UINib(nibName: "RefrehmentsCategoryViewCell", bundle: nil)
        refreshmentsCategoryCollectionView?.register(nib, forCellWithReuseIdentifier: RefrehmentsCategoryViewCell.reuseIdentifier)
        
        //set delegate properties
        refreshmentsCategoryCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        refreshmentsCategoryCollectionView?.delegate = self
        refreshmentsCategoryCollectionView?.dataSource = self
        
        if #available(iOS 13.0, *) {
            refreshmentsCategoryCollectionView!.backgroundColor = .systemBackground
        } else {
            refreshmentsCategoryCollectionView!.backgroundColor = .white
        }
        
        //add Subview
        view.addSubview(refreshmentsCategoryCollectionView!)
        refreshmentsCategoryCollectionView?.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        refreshmentsCategoryCollectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        refreshmentsCategoryCollectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        refreshmentsCategoryCollectionView!.heightAnchor.constraint(equalToConstant: 125).isActive = true
    }
    
    private func configureRefreshmentsFooterViewUI(){
        scrollView.addSubview(refreshmentsFooterView)
        refreshmentsFooterView.topAnchor.constraint(equalTo: refreshmentsCategoryView.bottomAnchor).isActive = true
        refreshmentsFooterView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        refreshmentsFooterView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        refreshmentsFooterView.heightAnchor.constraint(equalToConstant: 430).isActive = true
        refreshmentsFooterView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        //Add category label
        refreshmentsFooterView.addSubview(categoryNameLabel)
        categoryNameLabel.topAnchor.constraint(equalTo: refreshmentsFooterView.topAnchor, constant: 10).isActive = true
        categoryNameLabel.leadingAnchor.constraint(equalTo: refreshmentsFooterView.leadingAnchor, constant: 20).isActive = true
        categoryNameLabel.trailingAnchor.constraint(equalTo: refreshmentsFooterView.trailingAnchor, constant: -20).isActive = true
        
        setupRefreshmentItemsCollectionView(refreshmentsFooterView)
    }
    
    private func setupRefreshmentItemsCollectionView(_ view: UIView) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 5)
        refreshmentsItemsCollectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        refreshmentsItemsCollectionView?.showsHorizontalScrollIndicator = false
        
        //register Xib
        let nib = UINib(nibName: "RefreshmentItemsCollectionViewCell", bundle: nil)
        refreshmentsItemsCollectionView?.register(nib, forCellWithReuseIdentifier: RefreshmentItemsCollectionViewCell.reuseIdentifier)
        
        //set delegate properties
        refreshmentsItemsCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        refreshmentsItemsCollectionView?.delegate = self
        refreshmentsItemsCollectionView?.dataSource = self
        
        if #available(iOS 13.0, *) {
            refreshmentsItemsCollectionView!.backgroundColor = .systemBackground
        } else {
            refreshmentsItemsCollectionView!.backgroundColor = .white
        }
        
        //add Subview
        view.addSubview(refreshmentsItemsCollectionView!)
        refreshmentsItemsCollectionView?.topAnchor.constraint(equalTo: categoryNameLabel.bottomAnchor, constant: 15).isActive = true
        refreshmentsItemsCollectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        refreshmentsItemsCollectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        refreshmentsItemsCollectionView!.heightAnchor.constraint(equalToConstant: 345).isActive = true
    }
    
    private func configureMyMenuButtonUI(){
        view.addSubview(myMenuButton)
        myMenuButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        myMenuButton.widthAnchor.constraint(equalToConstant: 145).isActive = true
        myMenuButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        myMenuButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        myMenuButton.layer.cornerRadius = 30
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleMenuButtonTap(_:)))
        myMenuButton.addGestureRecognizer(tap)
        
        myMenuButton.dropShadow(shadowOpacity: 0.2, shadowRadius: 7, shadowColor: UIColor.black.cgColor)
        
        //Add menu icon
        myMenuButton.addSubview(myMenuIcon)
        myMenuIcon.heightAnchor.constraint(equalToConstant: 40).isActive = true
        myMenuIcon.widthAnchor.constraint(equalToConstant: 40).isActive = true
        myMenuIcon.leadingAnchor.constraint(equalTo: myMenuButton.leadingAnchor, constant: 10).isActive = true
        myMenuIcon.centerYAnchor.constraint(equalTo: myMenuButton.centerYAnchor).isActive = true
        myMenuIcon.layer.cornerRadius = 40/2
        
        //Add menu count label
        myMenuButton.addSubview(myMenuCountLabel)
        myMenuCountLabel.centerYAnchor.constraint(equalTo: myMenuButton.centerYAnchor).isActive = true
        myMenuCountLabel.trailingAnchor.constraint(equalTo: myMenuButton.trailingAnchor, constant: -17).isActive = true
        myMenuCountLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        myMenuCountLabel.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        
    }
    
    //MARK:- objc Methods
    @objc func handleMenuButtonTap(_ sender: UITapGestureRecognizer) {
        //print("menu button tapped")
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let refreshmentsCheckoutViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.REFRESHMENTS_CHECKOUT_CONTROLLER_ID)
        //self.present(searchSymptomsViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(refreshmentsCheckoutViewController, animated: true)
    }
    
    //MARK:- Firebase Methods
    private func fetchAllRefreshments(){
        refreshmentsCollectionRef.getDocuments { (snapshot, error) in
            if let err = error {
                debugPrint("Error fetching docs: \(err)")
            } else {
                guard let snap = snapshot else {return}
                for document in snap.documents {
                    let data = document.data()
                    
                    let refreshment_name = data["name"] as? String ?? ""
                    let refreshment_category = data["category"] as? String ?? ""
                    let refreshment_media_uri = data["media_uri"] as? String ?? ""
                    let refreshment_price = data["price"] as? Int ?? 0
                    
                    let newRefreshment = EventRefreshmentItem(id: document.documentID, name: refreshment_name, category: refreshment_category, media_uri: refreshment_media_uri, price: refreshment_price)
                    
                    self.refreshmentAllItems.append(newRefreshment)
                }
                self.refreshmentsItemsCollectionView?.reloadData()
                self.refreshmentSelectedCategoryItems = self.refreshmentAllItems
            }
        }
    }
    
    private func filterItemsOfCategory(category: String) {
        if category == "All" {
            refreshmentSelectedCategoryItems = refreshmentAllItems
        } else {
            //1- Clear all containing items
            refreshmentSelectedCategoryItems.removeAll()
            
            //2- Loop through every items of specific category
            for item in refreshmentAllItems {
                if item.category == category {
                    //3- Append if category found
                    refreshmentSelectedCategoryItems.append(item)
                }
            }
        }
        //4- Reload Collection view
        refreshmentsItemsCollectionView?.reloadData()
    }

}

//MARK:- Collection View DataSource And Delegate Methods
extension RefreshmentsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == refreshmentsCategoryCollectionView {
            return refreshmentCategories.count
        } else {
            return refreshmentSelectedCategoryItems.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView ==  refreshmentsCategoryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RefrehmentsCategoryViewCell.reuseIdentifier, for: indexPath) as! RefrehmentsCategoryViewCell
            cell.categoryNameLabel.text = refreshmentCategories[indexPath.row].name
            cell.categoryImageView.image = UIImage(named: refreshmentCategories[indexPath.row].imageName)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RefreshmentItemsCollectionViewCell.reuseIdentifier, for: indexPath) as! RefreshmentItemsCollectionViewCell
            cell.configureCell(item: refreshmentSelectedCategoryItems[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView ==  refreshmentsCategoryCollectionView {
            categoryNameLabel.text = refreshmentCategories[indexPath.row].name
            filterItemsOfCategory(category: refreshmentCategories[indexPath.row].name)
        } else {
            RefreshmentsCheckoutViewController.selectedCheckoutItems.append(refreshmentSelectedCategoryItems[indexPath.row])
            selectedRefreshmentItemsCount += 1
            myMenuCountLabel.text = "\(selectedRefreshmentItemsCount) items"
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == refreshmentsCategoryCollectionView {
            return CGSize(width: 98, height: 115)
        } else {
            return CGSize(width: 227, height: 330)
        }
    }
    
}
