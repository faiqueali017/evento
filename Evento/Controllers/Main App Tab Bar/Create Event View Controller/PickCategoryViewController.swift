//
//  PickCategoryViewController.swift
//  Evento
//
//  Created by Faiq on 22/04/2021.
//

import UIKit
import Firebase

class PickCategoryViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var pickCategoryTableView: UITableView!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    
    //MARK:- Variables
    private var event_categories = [EventCategory]()
    private var categoriesCollectionRef: CollectionReference!
    private var selectedCategoryIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePickEventCategoryScreen()
        fetchEventCategory()
    }
    
    //MARK:- IBActions
    @IBAction func didTapDoneBarButton(_ sender: UIBarButtonItem) {
        if doneBarButton.isEnabled {
            //Setting event category into struct
            EventConfirmViewController.newEvent.event_category = event_categories[selectedCategoryIndex].name
            
            //For presenting to next screen
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let pickEventViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.PICK_EVENT_VENUE_CONTROLLER_ID)
            //self.present(searchSymptomsViewController, animated: true, completion: nil)
            self.navigationController?.pushViewController(pickEventViewController, animated: true)
        }
    }
    
    //MARK:- Helper Methods
    private func configurePickEventCategoryScreen(){
        //Registering XIB
        pickCategoryTableView.register(UINib(nibName: "PickCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: PickCategoryTableViewCell.reuseIdentifier)
        
        doneBarButton.isEnabled = false
        
        //Setting firebase root ref
        categoriesCollectionRef = Firestore.firestore().collection(Constants.FIRESTORE_REF.EVENT_CATEGORY)
    }
    
    private func fetchEventCategory(){
        categoriesCollectionRef.getDocuments { (snapshot, error) in
            if let err = error {
                debugPrint("Error fetching docs: \(err)")
            } else {
                guard let snap = snapshot else {return}
                for document in snap.documents {
                    let data = document.data()
                    let category_name = data["name"] as? String ?? ""
                    let category_des = data["description"] as? String ?? ""
                    let category_media_uri = data["media_uri"] as? String ?? ""
                    
                    let newCategory = EventCategory(name: category_name, media_uri: category_media_uri, description: category_des)
                    
                    self.event_categories.append(newCategory)
                }
                self.pickCategoryTableView.reloadData()
            }
        }
    }

}

//MARK:- Extensions
extension PickCategoryViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event_categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PickCategoryTableViewCell.reuseIdentifier, for: indexPath) as! PickCategoryTableViewCell
        cell.configureCell(event_category: event_categories[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.doneBarButton.isEnabled = true
        PickEventVenueViewController.selectedEventCategory = event_categories[indexPath.row].name
        selectedCategoryIndex = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 290
    }
    
    
}
