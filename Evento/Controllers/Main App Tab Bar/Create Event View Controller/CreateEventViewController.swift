//
//  CreateEventViewController.swift
//  Evento
//
//  Created by Faiq on 08/04/2021.
//

import UIKit

class CreateEventViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStartButton()
    }
    
    //MARK:- Helper Methods
    private func setupStartButton(){
        //set image and set tint color
        let image = UIImage(named: "front-arrow-32")?.withRenderingMode(.alwaysTemplate)
        startButton.setImage(image, for: .normal)
        startButton.tintColor = UIColor.white
        
        //set corner radius and drop shadow
        startButton.layer.cornerRadius = 60/2
        startButton.dropShadow(shadowOpacity: 0.15, shadowRadius: 7, shadowColor: UIColor.black.cgColor)
    }

}
