//
//  PickEventVenueViewController.swift
//  Evento
//
//  Created by Faiq on 23/04/2021.
//

import UIKit
import Firebase

class PickEventVenueViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var venueNameLbl: UILabel!
    @IBOutlet weak var venueDescLbl: UILabel!
    @IBOutlet weak var venueTableView: UITableView!
    @IBOutlet weak var venueDoneBarButton: UIBarButtonItem!
    
    //MARK:- Variables
    static var selectedEventCategory = String()
    private var event_venues = [EventVenue]()
    private var venuesCollectionRef: CollectionReference!
    private var selectedVenueIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureEventVenueScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchEventVenue()
    }
    
    //MARK:- IBActions
    @IBAction func didTapVenueBarButton(_ sender: UIBarButtonItem) {
        if venueDoneBarButton.isEnabled {
            //Setting event venue into struct
            EventConfirmViewController.newEvent.event_venue_id = event_venues[selectedVenueIndex].id
            EventConfirmViewController.newEvent.event_venue_cost = event_venues[selectedVenueIndex].price
            
            //For presenting to next screen
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let chooseNoOfPersonsViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.CHOOSE_NO_OF_PERSONS_CONTROLLER_ID)
            //self.present(searchSymptomsViewController, animated: true, completion: nil)
            self.navigationController?.pushViewController(chooseNoOfPersonsViewController, animated: true)
            
        }
    }
    
    //MARK:- Helper Methods
    private func configureEventVenueScreen() {
        self.navigationItem.title = "Event Venue"
        
        venueNameLbl.text = PickEventVenueViewController.selectedEventCategory.firstCapitalized + " Venues"
        venueDoneBarButton.isEnabled = false
        
        venueTableView.register(UINib(nibName: "PickVenueTableViewCell", bundle: nil), forCellReuseIdentifier: PickVenueTableViewCell.reuseIdentifier)
        
        //Setting firebase root ref
        venuesCollectionRef = Firestore.firestore().collection(PickEventVenueViewController.selectedEventCategory)
    }
    
    private func fetchEventVenue(){
        venuesCollectionRef.getDocuments { (snapshot, error) in
            if let err = error {
                debugPrint("Error fetching docs: \(err)")
            } else {
                guard let snap = snapshot else {return}
                for document in snap.documents {
                    let data = document.data()
                    let venue_name = data["name"] as? String ?? ""
                    let venue_des = data["description"] as? String ?? ""
                    let venue_media_uri = data["media_uri"] as? String ?? ""
                    let venue_contact_no = data["contact_no"] as? String ?? ""
                    let venue_address = data["address"] as? String ?? ""
                    let venue_coordinates = data["coordinates"] as? GeoPoint ?? GeoPoint(latitude: 0.000000, longitude: 0.000000)
                    let venue_price = data["price"] as? Int ?? 0
                    let venue_timings = data["timings"] as? String ?? ""
                    
                    let newCategory = EventVenue(id: document.documentID, name: venue_name, address: venue_address, contact_no: venue_contact_no, description: venue_des, media_uri: venue_media_uri, price: venue_price, timings: venue_timings, coordinates: venue_coordinates)
                    
                    self.event_venues.append(newCategory)
                }
                self.venueTableView.reloadData()
            }
        }
    }
    
}

//MARK:- Extensions
extension PickEventVenueViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event_venues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PickVenueTableViewCell.reuseIdentifier, for: indexPath) as! PickVenueTableViewCell
        cell.configureCell(venue: event_venues[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.venueDoneBarButton.isEnabled = true
        EventVenueDetailViewController.selectedEventVenue = event_venues[indexPath.row]
        selectedVenueIndex = indexPath.row
        present(EventVenueDetailViewController(), animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
}

