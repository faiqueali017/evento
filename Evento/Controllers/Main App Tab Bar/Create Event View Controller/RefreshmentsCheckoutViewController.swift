//
//  RefreshmentsCheckoutViewController.swift
//  Evento
//
//  Created by Faiq on 25/04/2021.
//

import UIKit

class RefreshmentsCheckoutViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkoutBackgroundView: UIView!
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var noOfItemsLabel: UILabel!
    
    //MARK:- Varibales
    static var selectedCheckoutItems = [EventRefreshmentItem]()
    var itemsCount = RefreshmentCategoryCount(starter: 0, mainCourse: 0, drinks: 0, dessert: 0)
    let persons = EventConfirmViewController.newEvent.event_persons
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshmentsCheckoutScreen()
    }
    
    //MARK:- IBActions
    @IBAction func didTapCheckoutButton(_ sender: Any) {
        settingUpRefreshmentsCheckoutForFirebase(items: RefreshmentsCheckoutViewController.selectedCheckoutItems)
        
        //Presenting new screen
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.BILL_CHECKOUT_CONTROLLER_ID)
        //self.present(searchSymptomsViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Helper Methods
    private func setupRefreshmentsCheckoutScreen(){
        //Registering XIB
        tableView.register(UINib(nibName: "RefreshmentsCheckoutTableViewCell", bundle: nil), forCellReuseIdentifier: RefreshmentsCheckoutTableViewCell.reuseIdentifier)
        
        //Checkout button UI Setting
        //checkoutBackgroundView.dropShadow(shadowOpacity: 0.5, shadowRadius: 12, shadowColor: UIColor.black.cgColor)
        checkoutButton.layer.cornerRadius = 50/2
       
        //Calculating items per category and their cost
        countItemsPerCategory(items: RefreshmentsCheckoutViewController.selectedCheckoutItems)
        calculateItemsPricePerPerson(items: RefreshmentsCheckoutViewController.selectedCheckoutItems)
        
        //Setting up items count label
        noOfItemsLabel.text = String(RefreshmentsCheckoutViewController.selectedCheckoutItems.count)
        
    }
    
    private func countItemsPerCategory(items: [EventRefreshmentItem]){
        for item in items {
            if item.category == "Starter" {
                itemsCount.starter += 1
            } else if item.category == "Burgers" || item.category == "Noodles" {
                itemsCount.mainCourse += 1
            } else if item.category == "Dessert" {
                itemsCount.dessert += 1
            } else if item.category == "Drinks" || item.category == "Hot Drinks"{
                itemsCount.drinks += 1
            }
        }
    }
    
    private func calculateItemsPricePerPerson(items: [EventRefreshmentItem]) {
        var totalRefreshmentCost = 0
        
        for i in 0..<items.count {
            if items[i].category == "Starter" {
                if itemsCount.starter > 0 {
                    RefreshmentsCheckoutViewController.selectedCheckoutItems[i].price = items[i].price * (persons/itemsCount.starter)
                }
            } else if items[i].category == "Burgers" || items[i].category == "Noodles" {
                if itemsCount.mainCourse > 0 {
                    RefreshmentsCheckoutViewController.selectedCheckoutItems[i].price = items[i].price * (persons/itemsCount.mainCourse)
                }
            } else if items[i].category == "Dessert" {
                if itemsCount.dessert > 0 {
                    RefreshmentsCheckoutViewController.selectedCheckoutItems[i].price = items[i].price * (persons/itemsCount.dessert)
                }
            } else if items[i].category == "Drinks" || items[i].category == "Hot Drinks"{
                if itemsCount.drinks > 0 {
                    RefreshmentsCheckoutViewController.selectedCheckoutItems[i].price = items[i].price * (persons/itemsCount.drinks)
                }
            }
            
            //Add item price in total cost
            totalRefreshmentCost += totalRefreshmentCost + RefreshmentsCheckoutViewController.selectedCheckoutItems[i].price
        }
        totalCostLabel.text = String(totalRefreshmentCost)
    }
    
    private func settingUpRefreshmentsCheckoutForFirebase(items: [EventRefreshmentItem]){
        var refreshmentsID = [String]()
        
        for i in 0..<items.count{
            refreshmentsID.append(items[i].id)
        }
        
        //Setting event refreshments details into struct
        EventConfirmViewController.newEvent.event_refreshments_id = refreshmentsID
        EventConfirmViewController.newEvent.event_refreshments_cost = Utilities.double(totalCostLabel)
        EventConfirmViewController.newEvent.event_refreshments_items_count = Utilities.integer(noOfItemsLabel)
        
    }
    
}

//MARK:- Extensions
extension RefreshmentsCheckoutViewController:
    UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RefreshmentsCheckoutViewController.selectedCheckoutItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RefreshmentsCheckoutTableViewCell.reuseIdentifier, for: indexPath) as! RefreshmentsCheckoutTableViewCell
        cell.configureCell(item: RefreshmentsCheckoutViewController.selectedCheckoutItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            RefreshmentsCheckoutViewController.selectedCheckoutItems.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}
