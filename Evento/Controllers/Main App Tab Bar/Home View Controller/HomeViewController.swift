//
//  HomeViewController.swift
//  Evento
//
//  Created by Faiq on 02/04/2021.
//

import UIKit

class HomeViewController: UIViewController {
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.showsVerticalScrollIndicator = false
        //view.backgroundColor = .systemBackground
        return view
    }()
    
    /// FEATURED VIEW
    private let featuredView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .red
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    lazy var view0: UIView = {
        let view = UIView()
        view.backgroundColor = .systemTeal
        
        let label = UILabel()
        label.text = "page 0"
        label.textAlignment = .center
        view.addSubview(label)
        //label.edgeTo(view: view)
        
        return view
    }()
    
    lazy var view1: UIView = {
        let view = UIView()
        view.backgroundColor = .systemPink
        
        let label = UILabel()
        label.text = "page 1"
        label.textAlignment = .center
        view.addSubview(label)
        //label.edgeTo(view: view)
        
        return view
    }()
    
    lazy var view2: UIView = {
        let view = UIView()
        view.backgroundColor = .systemYellow
        
        let label = UILabel()
        label.text = "page 2"
        label.textAlignment = .center
        view.addSubview(label)
        //label.edgeTo(view: view)
        
        return view
    }()
    
    lazy var views = [view0, view1, view2]
    
    lazy var featureScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: view.frame.height)
        for i in 0..<views.count {
            scrollView.addSubview(views[i])
            views[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
        }
        scrollView.delegate = self
        return scrollView
    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = views.count
        pageControl.currentPage = 0
        pageControl.addTarget(self, action: #selector(pageControlTapHandler(sender:)), for: .touchUpInside)
        return pageControl
    }()
    
    @objc func pageControlTapHandler(sender: UIPageControl) {
        scrollView.scrollTo(horizontalPage: sender.currentPage, animated: true)
    }
    
    
    ///DISCOUNT VIEW
    private let discountOffersView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private var discountOffersCollectionView: UICollectionView?
    
    let discountOfferLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "Discount Offers"
    
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        
        return label
    }()
    
    ///TRENDING VIEW
    private let trendingEventsView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let trendingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "Trending"
    
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        
        return label
    }()
    
    private var trendingEventsCollectionView: UICollectionView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.setNavigationBarHidden(true, animated: animated)
        //self.navigationController?.navigationBar.prefersLargeTitles = true
        //self.navigationItem.title = "Home"
    }
    

    //MARK:- Helper Methods
    private func setupUI(){
        //adding scroll view as a main
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        ///-----------------FEATURED VIEW---------------------
        scrollView.addSubview(featuredView)
        featuredView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        featuredView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        featuredView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        featuredView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        featuredView.addSubview(featureScrollView)
        featureScrollView.edgeTo(view: featuredView)
        
        featuredView.addSubview(pageControl)
        pageControl.pinTo(featuredView)
        
        
        ///-----------------DISCOUNT OFFERS VIEW---------------------
        scrollView.addSubview(discountOffersView)
        discountOffersView.topAnchor.constraint(equalTo: featuredView.bottomAnchor).isActive = true
        discountOffersView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        discountOffersView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        discountOffersView.heightAnchor.constraint(equalToConstant: 210).isActive = true
        
        //add discount label
        discountOffersView.addSubview(discountOfferLabel)
        discountOfferLabel.topAnchor.constraint(equalTo: discountOffersView.topAnchor, constant: 5).isActive = true
        discountOfferLabel.leadingAnchor.constraint(equalTo: discountOffersView.leadingAnchor, constant: 15).isActive = true
        discountOfferLabel.trailingAnchor.constraint(equalTo: discountOffersView.trailingAnchor, constant: -15).isActive = true
        discountOfferLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        //add discount collection view
        setupDiscountOffersCollectionView(discountOffersView)

        
        ///-----------------TRENDING VIEW---------------------
        scrollView.addSubview(trendingEventsView)
        trendingEventsView.topAnchor.constraint(equalTo: discountOffersView.bottomAnchor).isActive = true
        trendingEventsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trendingEventsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        trendingEventsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        trendingEventsView.heightAnchor.constraint(equalToConstant: (213 * 5) + 70).isActive = true

        //add trending label
        trendingEventsView.addSubview(trendingLabel)
        trendingLabel.topAnchor.constraint(equalTo: trendingEventsView.topAnchor, constant: 5).isActive = true
        trendingLabel.leadingAnchor.constraint(equalTo: trendingEventsView.leadingAnchor, constant: 15).isActive = true
        trendingLabel.trailingAnchor.constraint(equalTo: trendingEventsView.trailingAnchor, constant: -15).isActive = true
        trendingLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        //add trending venue collection view
        setupTrendingEventsCollectionView(trendingEventsView)
        
        
    }
    
    
    private func setupDiscountOffersCollectionView(_ discountOfferView: UIView) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 0, right: 5)
        discountOffersCollectionView = UICollectionView(frame: discountOffersView.frame, collectionViewLayout: layout)
        discountOffersCollectionView?.showsHorizontalScrollIndicator = false
        
        //register Xib
        let nib = UINib(nibName: "DiscountOffersCollectionViewCell", bundle: nil)
        discountOffersCollectionView?.register(nib, forCellWithReuseIdentifier: DiscountOffersCollectionViewCell.reuseIdentifier)
        
        //set delegate properties
        discountOffersCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        discountOffersCollectionView?.delegate = self
        discountOffersCollectionView?.dataSource = self
        
        if #available(iOS 13.0, *) {
            discountOffersCollectionView!.backgroundColor = .systemBackground
        } else {
            discountOffersCollectionView!.backgroundColor = .white
        }
        
        //add Subview
        discountOffersView.addSubview(discountOffersCollectionView!)
        discountOffersCollectionView?.topAnchor.constraint(equalTo: discountOfferLabel.bottomAnchor, constant: 5).isActive = true
        discountOffersCollectionView?.leadingAnchor.constraint(equalTo: discountOffersView.leadingAnchor).isActive = true
        discountOffersCollectionView?.trailingAnchor.constraint(equalTo: discountOffersView.trailingAnchor).isActive = true
        discountOffersCollectionView!.heightAnchor.constraint(equalToConstant: 160).isActive = true
    }
    
    private func setupTrendingEventsCollectionView(_ trendingEventsView: UIView){
        //TrendingEventsCollectionViewCell
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
        trendingEventsCollectionView = UICollectionView(frame: discountOffersView.frame, collectionViewLayout: layout)
        trendingEventsCollectionView?.showsHorizontalScrollIndicator = false
        trendingEventsCollectionView?.isScrollEnabled = false
        
        //register Xib
        let nib = UINib(nibName: "TrendingEventsCollectionViewCell", bundle: nil)
        trendingEventsCollectionView?.register(nib, forCellWithReuseIdentifier: TrendingEventsCollectionViewCell.reuseIdentifier)
        
        //set delegate properties
        trendingEventsCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        trendingEventsCollectionView?.delegate = self
        trendingEventsCollectionView?.dataSource = self
        
        if #available(iOS 13.0, *) {
            trendingEventsCollectionView!.backgroundColor = .systemBackground
        } else {
            trendingEventsCollectionView!.backgroundColor = .white
        }
        
        //add Subview
        trendingEventsView.addSubview(trendingEventsCollectionView!)
        trendingEventsCollectionView?.topAnchor.constraint(equalTo: trendingLabel.bottomAnchor, constant: 5).isActive = true
        trendingEventsCollectionView?.leadingAnchor.constraint(equalTo: trendingEventsView.leadingAnchor).isActive = true
        trendingEventsCollectionView?.trailingAnchor.constraint(equalTo: trendingEventsView.trailingAnchor).isActive = true
        trendingEventsCollectionView!.heightAnchor.constraint(equalToConstant: (213 * 5)+15).isActive = true
//        trendingEventsCollectionView?.bottomAnchor.constraint(equalTo: trendingEventsView.bottomAnchor, constant: 10).isActive = true
    }

}


//MARK:- Collection View DataSource And Delegate Methods
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == discountOffersCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DiscountOffersCollectionViewCell.reuseIdentifier, for: indexPath) as! DiscountOffersCollectionViewCell
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TrendingEventsCollectionViewCell.reuseIdentifier, for: indexPath) as! TrendingEventsCollectionViewCell
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == discountOffersCollectionView {
            return CGSize(width: 120, height: 155)
        } else {
            return CGSize(width: view.frame.width - 30, height: 213)
        }
        
    }
    
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
