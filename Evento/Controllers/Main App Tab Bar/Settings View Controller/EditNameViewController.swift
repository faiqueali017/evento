//
//  EditNameViewController.swift
//  Evento
//
//  Created by Faiq on 26/04/2021.
//

import UIKit

class EditNameViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var nameEditTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    //MARK:- Variables
    var textFieldText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.isEnabled = false
        hideKeyboardWhenTappedAround()
        nameEditTextField.text = UserDefaults.standard.value(forKey: "USERNAME") as? String
    }
    
    //MARK:- IBActions
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        UserDefaults.standard.setValue(nameEditTextField.text, forKey: "USERNAME")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NAMEEDITED"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        if sender.text == textFieldText {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }
    }

}
