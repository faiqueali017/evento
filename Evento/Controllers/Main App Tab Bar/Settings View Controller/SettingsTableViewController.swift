//
//  SettingsTableViewController.swift
//  Evento
//
//  Created by Faiq on 14/04/2021.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var profilePicImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicImageView.layer.cornerRadius = 75/2
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            print("Profile edit")
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileEditViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.PROFILE_EDIT_CONTROLLER_ID)
            //self.present(profileEditViewController, animated: true, completion: nil)
            self.navigationController?.pushViewController(profileEditViewController, animated: true)
        }
        else if indexPath.section == 3 && indexPath.row == 0 {
            //after successfull logout
            UserDefaults.standard.removeObject(forKey: Constants.USER_DEFAULTS.KEYS.USER_SIGNED_IN)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.LOGIN_NAVIGATION_CONTROLLER)
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        }
    }
}
