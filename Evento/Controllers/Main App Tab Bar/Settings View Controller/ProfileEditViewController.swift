//
//  ProfileEditViewController.swift
//  Evento
//
//  Created by Faiq on 26/04/2021.
//

import UIKit

class ProfileEditViewController: UIViewController {

    /// PROFILE EDIT IMAGE VIEW
    private let profileEditHeaderView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = #colorLiteral(red: 0.864036262, green: 0.8641816974, blue: 0.8640171885, alpha: 1)
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let profileImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        //image.image = UIImage(named: "turtlerock")
        image.backgroundColor = .systemBackground
        image.layer.masksToBounds = false
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private let editImageButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
        return button
    }()
    
    private let editProfilePicLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 17)
        label.text = "Tap on 'Edit' to change your profile picture."
        label.numberOfLines = 0
        label.textAlignment = .center
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    /// PROFILE EDIT NAME VIEW
    private let profileEditNameView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = #colorLiteral(red: 0.864036262, green: 0.8641816974, blue: 0.8640171885, alpha: 1)
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let editNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Medium", size: 20)
        label.text = UserDefaults.standard.value(forKey: "USERNAME") as? String
        label.numberOfLines = 0
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let editNameButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "edit-name.png"), for: .normal)
        button.addTarget(self, action: #selector(didTapEditNameButton), for: .touchUpInside)
        button.imageView?.image?.withTintColor(.systemBlue)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Edit Profile"
        navigationController?.navigationBar.prefersLargeTitles = false
        
        setupUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUsername), name: NSNotification.Name(rawValue: "NAMEEDITED"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        view.addSubview(profileEditHeaderView)
        profileEditHeaderView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 40).isActive = true
        profileEditHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        profileEditHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        profileEditHeaderView.heightAnchor.constraint(equalToConstant: 135).isActive = true
        
        profileEditHeaderView.addSubview(profileImageView)
        profileImageView.topAnchor.constraint(equalTo: profileEditHeaderView.topAnchor, constant: 20).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo: profileEditHeaderView.leadingAnchor, constant: 20).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        profileImageView.layer.cornerRadius = 70/2
        
        profileEditHeaderView.addSubview(editProfilePicLabel)
        editProfilePicLabel.centerYAnchor.constraint(equalTo: profileEditHeaderView.centerYAnchor).isActive = true
        editProfilePicLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 20).isActive = true
        editProfilePicLabel.trailingAnchor.constraint(equalTo: profileEditHeaderView.trailingAnchor, constant: -20).isActive = true
        
        profileEditHeaderView.addSubview(editImageButton)
        editImageButton.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 5).isActive = true
        editImageButton.leadingAnchor.constraint(equalTo: profileEditHeaderView.leadingAnchor, constant: 20).isActive = true
        editImageButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        editImageButton.widthAnchor.constraint(equalToConstant: 68).isActive = true
        
        
        //ADD EDIT NAME VIEW
        view.addSubview(profileEditNameView)
        profileEditNameView.topAnchor.constraint(equalTo: profileEditHeaderView.bottomAnchor, constant: 30).isActive = true
        profileEditNameView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        profileEditNameView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        profileEditNameView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        //Add Edit Btn
        profileEditNameView.addSubview(editNameButton)
        editNameButton.centerYAnchor.constraint(equalTo: profileEditNameView.centerYAnchor).isActive = true
        editNameButton.trailingAnchor.constraint(equalTo: profileEditNameView.trailingAnchor, constant: -25).isActive = true
        editNameButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        editNameButton.widthAnchor.constraint(equalToConstant: 25).isActive = true

        
        //Add label
        profileEditNameView.addSubview(editNameLabel)
        editNameLabel.centerYAnchor.constraint(equalTo: profileEditNameView.centerYAnchor).isActive = true
        editNameLabel.leadingAnchor.constraint(equalTo: profileEditNameView.leadingAnchor, constant: 30).isActive = true
        editNameLabel.trailingAnchor.constraint(equalTo: editNameButton.leadingAnchor, constant: -20).isActive = true
        editNameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
    }
    
    @IBAction func didTapSaveProfileButton(_ sender: UIBarButtonItem) {
        //Initialize a Hud view obj
        let hudView = HudView.hud(inView: self.view, animated: true)
        
        //Set hud view text
        hudView.text = NSLocalizedString("Saved", comment: "Saved")
        
        //Dismiss hud view timings
        hudView.afterDelay(0.8) {
            hudView.hide()
            self.navigationController?.popViewController(animated: true)        }
    }
    
    //MARK:- objc Methods
    @objc func didTapEditButton(){
        self.presentPhotoActionSheet()
    }
    
    @objc func didTapEditNameButton(){
        print("Edit name tapped")
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editNameViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.EDIT_NAME_CONTROLLER_ID)

        self.present(editNameViewController, animated: true, completion: nil)
    }
    
    @objc func updateUsername(){
        editNameLabel.text = UserDefaults.standard.value(forKey: "USERNAME") as? String
        
    }

}

//MARK:- Extension Image Picker
extension ProfileEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
         
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel,
                                            handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Take Photo",
                                            style: .default,
                                            handler: { [weak self] _ in
                                                self?.showImagePickerController(sourceType: .camera)
                                            }))
        
        actionSheet.addAction(UIAlertAction(title: "Choose Photo",
                                            style: .default,
                                            handler: { [weak self] _ in
                                                self?.showImagePickerController(sourceType: .photoLibrary)
                                            }))
        
        present(actionSheet, animated: true)
    }
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImageView.image = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = originalImage
        }
        
        picker.dismiss(animated: true, completion: nil)

    }

}
