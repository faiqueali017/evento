//
//  LoginViewController.swift
//  Evento
//
//  Created by Faiq on 02/04/2021.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    //MARK:- Constants
    private let isUserSignedInDefaults = UserDefaults.standard
    private let isAdminSignedInDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        
    }
    
    //MARK:- IBActions
    @IBAction func signInBtnTapped(_ sender: Any) {
        //validate fields
        let error = validateFields()
        
        if error != nil {
            //There's something wrong, show error message
            showError(error!)
            
        } else {
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //sign in
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error != nil {
                    //Couldn't signed in
                    self.showError("Error signing in")
                } else {
                    //Admin is logging in
                    if email == Constants.ADMIN_CREDENTIALS.EMAIL && password == Constants.ADMIN_CREDENTIALS.PASSWORD{

                        //transition to admin bar
                        self.transitionToAdminTabBarController()
                        
                        //save admin signed state to user defaults
                        self.isAdminSignedInDefaults.set(Constants.APP_STATE_PROPERTIES.ADMIN_SIGNED_IN, forKey: Constants.USER_DEFAULTS.KEYS.ADMIN_SIGNED_IN)
                    } else {
                        //transition to Tab Bar Controller
                        self.transitionToTabBarController()
                        
                        //save user signed state to user defaults
                        self.isUserSignedInDefaults.set(Constants.APP_STATE_PROPERTIES.USER_SIGNED_IN, forKey: Constants.USER_DEFAULTS.KEYS.USER_SIGNED_IN)
                    }  
                }
            }
        }
        
    }
    
    //MARK:- Helper Methods
    private func setupElements(){
        //hide the error label
        errorLabel.alpha = 0
        
        //styling text fields
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signInBtn)
    }
    
    /*
     Check fields and validate that the data is correct. If everything is correct, return nil. Otherwise return the error.
     */
    private func validateFields() -> String? {
        
        //check all that fields are nil
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        
        //check if password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            //password isn't secured
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    private func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    private func transitionToTabBarController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarContoller = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.TAB_BAR_CONTROLLER_ID)
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarContoller)
    }
    
    private func transitionToAdminTabBarController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let adminTabBarContoller = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.ADMIN_TAB_CONTROLLER_ID)
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(adminTabBarContoller)
    }
    
}
