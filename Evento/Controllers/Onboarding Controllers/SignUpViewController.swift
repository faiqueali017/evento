//
//  SignUpViewController.swift
//  Evento
//
//  Created by Faiq on 02/04/2021.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    //MARK:- Constants
    private let isUserSignedInDefaults = UserDefaults.standard
    private let isAdminSignedInDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
    }
    
    //MARK:- IBActions
    @IBAction func signUpTapped(_ sender: Any) {
        //validate fields
        let error = validateFields()
        
        if error != nil {
            //There's something wrong, show error message
            showError(error!)
            
        } else {
            //create cleaned version of the data
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastnameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //create user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                
                //Check for errors
                if err != nil {
                    // There was an error creating the user
                    self.showError("Error creating user")
                } else {
                    //User was created successfully
                    let db = Firestore.firestore()
                    
                    db.collection("users").document(result!.user.uid).setData(
                        ["uid": result!.user.uid,
                         "firstname": firstName,
                         "lastname": lastName,
                         "email": email ]) { error in
                        
                        if error != nil {
                            //show error message
                            self.showError("Error saving user data")
                        }
                    }
                    
                    //Admin is signing up
                    if email == Constants.ADMIN_CREDENTIALS.EMAIL && password == Constants.ADMIN_CREDENTIALS.PASSWORD{

                        //transition to admin bar
                        self.transitionToAdminTabBarController()
                        
                        //save admin signed state to user defaults
                        self.isAdminSignedInDefaults.set(Constants.APP_STATE_PROPERTIES.ADMIN_SIGNED_IN, forKey: Constants.USER_DEFAULTS.KEYS.ADMIN_SIGNED_IN)
                    } else {
                        //transition to Tab Bar Controller
                        self.transitionToTabBarController()
                        
                        //save user signed state to user defaults
                        self.isUserSignedInDefaults.set(Constants.APP_STATE_PROPERTIES.USER_SIGNED_IN, forKey: Constants.USER_DEFAULTS.KEYS.USER_SIGNED_IN)
                    }
                }
            }
            
        }
        
    }
    
    
    //MARK:- Helper Methods
    private func setupElements(){
        //hide the error label
        errorLabel.alpha = 0
        
        //styling text fields
        Utilities.styleTextField(firstNameTextField)
        Utilities.styleTextField(lastnameTextField)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signUpBtn)
    }
    
    /*
     Check fields and validate that the data is correct. If everything is correct, return nil. Otherwise return the error.
     */
    private func validateFields() -> String? {
        
        //check all that fields are nil
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        
        //check if password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            //password isn't secured
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    private func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    private func transitionToTabBarController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarContoller = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.TAB_BAR_CONTROLLER_ID)
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarContoller)
    }
    
    private func transitionToAdminTabBarController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let adminTabBarContoller = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.ADMIN_TAB_CONTROLLER_ID)
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(adminTabBarContoller)
    }
    
}
