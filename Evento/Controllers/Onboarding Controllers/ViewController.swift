//
//  ViewController.swift
//  Evento
//
//  Created by Faiq on 02/04/2021.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //MARK:- IBActions
    @IBAction func signUpBtnTapped(_ sender: Any) {
        
    }
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        
    }
    
    //MARK:- Helper Methods
    private func setupElements(){
        //styling text fields
        Utilities.styleFilledButton(signUpBtn)
        Utilities.styleHollowButton(loginBtn)
    }
    
}

