//
//  DiscountOffersCollectionViewCell.swift
//  Evento
//
//  Created by Faiq on 08/04/2021.
//

import UIKit

class DiscountOffersCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var venueDiscountLabel: UILabel!
    
    //MARK:- Constants
    static let reuseIdentifier = "DiscountOffersCollectionViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        venueImageView.layer.cornerRadius = 13
    }

}
