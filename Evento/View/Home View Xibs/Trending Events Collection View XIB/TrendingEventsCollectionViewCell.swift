//
//  TrendingEventsCollectionViewCell.swift
//  Evento
//
//  Created by Faiq on 08/04/2021.
//

import UIKit

class TrendingEventsCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Constants
    static var reuseIdentifier = "TrendingEventsCollectionViewCell"
    
    //MARK:- Outlets
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventNamelabel: UILabel!
    @IBOutlet weak var eventDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        eventImageView.layer.cornerRadius = 10
    }

}
