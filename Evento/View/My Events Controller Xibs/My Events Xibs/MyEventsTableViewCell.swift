//
//  MyEventsTableViewCell.swift
//  Evento
//
//  Created by Faiq on 02/05/2021.
//

import UIKit
import Firebase

class MyEventsTableViewCell: UITableViewCell {
    
    //MARK:- Constants
    static let reuseIdentifier = "MyEventsTableViewCell"
    private let db = Firestore.firestore()
    
    //MARK:- IBOutlets
    @IBOutlet weak var eventImageBackgroundView: UIView!
    @IBOutlet weak var eventNamelabel: UILabel!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventDateLabel: UILabel!
    
    //MARK:- Variables
    private var ref: CollectionReference!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        eventImageBackgroundView.layer.cornerRadius = 100/2
        eventImageView.layer.cornerRadius = 97/2
        
        eventImageBackgroundView.dropShadow(shadowOpacity: 0.2, shadowRadius: 9, shadowColor: UIColor.black.cgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(event: NewEvent) {
        eventNamelabel.text = event.event_name
        eventDateLabel.text = Utilities.convertDateTimeFromTimeStamp(timestamp: event.event_timestamp)
        populateVenueImage(event.event_category, event.event_venue_id, eventImageView)
    }
    
    func populateVenueImage(_ eventCategory: String, _ venueID: String, _ imageView: UIImageView) {
        
        switch eventCategory {
        case "picnic":
            ref = db.collection(Constants.FIRESTORE_REF.PICNIC)
        case "conference":
            ref = db.collection(Constants.FIRESTORE_REF.CONFERENCE)
        case "meeting":
            ref = db.collection(Constants.FIRESTORE_REF.MEETING)
        case "concert":
            ref = db.collection(Constants.FIRESTORE_REF.CONFERENCE)
        default:
            ref = db.collection("")
        }
        
        ref.getDocuments { (snapshot, error) in
            if let err = error {
                debugPrint("Error fetching docs: \(err)")
            } else {
                guard let snap = snapshot else {return}
                for document in snap.documents {
                    if document.documentID == venueID {
                        let data = document.data()
                        let url = data["media_uri"] as? String ?? ""
                        imageView.loadImageFromUrlString(urlString: url)
                    }
                }
            }
        }
        
    }
    
}
