//
//  NoEventsTableViewCell.swift
//  Evento
//
//  Created by Faiq on 02/05/2021.
//

import UIKit

class NoEventsTableViewCell: UITableViewCell {
    
    //MARK:- Constant
    static let reuseIdentifier = "NoEventsTableViewCell"
    
    //MARK:- IBOutlets
    @IBOutlet weak var noEventsDescLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureEventDescLabelTextForAdmin(){
        noEventsDescLabel.text = "You have no pending events till now. Wait until you got some events!!"
    }
    
}
