//
//  RefrehmentsCategoryViewCell.swift
//  Evento
//
//  Created by Faiq on 25/04/2021.
//

import UIKit

class RefrehmentsCategoryViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "RefrehmentsCategoryViewCell"

    @IBOutlet weak var categoryImageBackgroundView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        categoryImageBackgroundView.layer.cornerRadius = 25
    }

}
