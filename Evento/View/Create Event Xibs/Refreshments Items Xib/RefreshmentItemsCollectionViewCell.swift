//
//  RefreshmentItemsCollectionViewCell.swift
//  Evento
//
//  Created by Faiq on 25/04/2021.
//

import UIKit

class RefreshmentItemsCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Constants
    static let reuseIdentifier = "RefreshmentItemsCollectionViewCell"

    //MARK:- IBOutlets
    @IBOutlet weak var itemMainBackgroundView: UIView!
    @IBOutlet weak var itemImageBackgrounView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var addMenuBackgroundView: UIView!
    @IBOutlet weak var addMenuImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemServingLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemMainBackgroundView.layer.cornerRadius = 32
        itemImageBackgrounView.layer.cornerRadius = 140/2
        itemImageView.layer.cornerRadius = 130/2
        
        addMenuBackgroundView.layer.cornerRadius = 15
        addMenuImageView.layer.cornerRadius = 27/2
        
        itemImageBackgrounView.dropShadow(shadowOpacity: 0.28, shadowRadius: 7, shadowColor: UIColor.black.cgColor)
        
    }
    
    func configureCell(item: EventRefreshmentItem){
        itemNameLabel.text = item.name
        itemPriceLabel.text = String(item.price)
        itemImageView.loadImageFromUrlString(urlString: item.media_uri)
//        if let imageURL = URL(string: item.media_uri) {
//            itemImageView.loadImage(url: imageURL)
//        }
    }

}
