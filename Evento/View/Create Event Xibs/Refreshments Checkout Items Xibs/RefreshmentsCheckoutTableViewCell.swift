//
//  RefreshmentsCheckoutTableViewCell.swift
//  Evento
//
//  Created by Faiq on 01/05/2021.
//

import UIKit

class RefreshmentsCheckoutTableViewCell: UITableViewCell {
    
    //MARK:- Constant
    static let reuseIdentifier = "RefreshmentsCheckoutTableViewCell"

    //MARK:- IBOutlets
    @IBOutlet weak var itemBackgroundView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var ItemServingLabel: UILabel!
    @IBOutlet weak var itemTotalCost: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemImageView.layer.cornerRadius = 76/2
        itemBackgroundView.layer.cornerRadius = 30
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(item: EventRefreshmentItem){
        itemNameLabel.text = item.name
        itemTotalCost.text = String(item.price)
        itemImageView.loadImageFromUrlString(urlString: item.media_uri)
//        if let imageURL = URL(string: item.media_uri) {
//            itemImageView.loadImage(url: imageURL)
//        }
    }
    
}
