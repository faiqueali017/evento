//
//  PickCategoryTableViewCell.swift
//  Evento
//
//  Created by Faiq on 22/04/2021.
//

import UIKit

class PickCategoryTableViewCell: UITableViewCell {

    //MARK:- Constant
    static let reuseIdentifier = "PickCategoryTableViewCell"
    
    //MARK:- IBOutlets
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryDescLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryImageView.layer.cornerRadius = 15
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(event_category: EventCategory) {
        categoryNameLabel.text = event_category.name.firstCapitalized
        categoryDescLabel.text = event_category.description
        categoryImageView.loadImageFromUrlString(urlString: event_category.media_uri)
//        if let imageURL = URL(string: event_category.media_uri) {
//            categoryImageView.loadImage(url: imageURL)
//        }
    }
}
