//
//  PickVenueTableViewCell.swift
//  Evento
//
//  Created by Faiq on 23/04/2021.
//

import UIKit

class PickVenueTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "PickCategoryTableViewCell"

    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var venueNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(venue: EventVenue){
        venueNameLabel.text = venue.name
        venueImageView.loadImageFromUrlString(urlString: venue.media_uri)
//        if let imageURL = URL(string: venue.media_uri) {
//            venueImageView.loadImage(url: imageURL)
//        }
    }
}
